# -*- coding: utf-8 -*-
# vim: ft=sls

{% from "sudoers/map.jinja" import sudoers with context %}

sssd:
  pkg.installed:
    - name: sudo
    - refresh: True

sudoers_admin:
  file.managed:
    - name: /etc/sudoers.d/admin
    - source: salt://sssd/files/sudoers-admin.j2
    - template: jinja
    - user: root
    - group: root
    - mode: 640

