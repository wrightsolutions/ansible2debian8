# -*- coding: utf-8 -*-
# vim: ft=sls

{% from "sudoers/map.jinja" import sudoers with context %}

sudoers_pkg_sudo:
  pkg.installed:
    - name: sudo
    - refresh: True

include:
  - sudoers.sudoers_alias
  - sudoers.sudoers_group_user


