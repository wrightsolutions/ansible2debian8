# -*- coding: utf-8 -*-
# vim: ft=sls

sudoers_d_group:
  file.managed:
    - name: /etc/sudoers.d/sudoGroup
    - source: salt://sudoers/files/sudoGroup.j2
    - template: jinja
    - user: root
    - group: root
    - mode: 640
    - makedirs: False

sudoers_d_user:
  file.managed:
    - name: /etc/sudoers.d/sudoUser
    - source: salt://sudoers/files/sudoUser.j2
    - template: jinja
    - user: root
    - group: root
    - mode: 640
    - makedirs: False

